package music.band;

import music.product.AbstractDrummer;
import music.product.AbstractGuitarist;
import music.product.AbstractVocalist;
import music.product.Artist;

/**
 * Created by prayagupd
 * on 4/14/15.
 */

public class Band {
  private AbstractDrummer drummer;
  private AbstractGuitarist guitarist;
  private AbstractVocalist vocalist;

  public AbstractDrummer getDrummer() {
    return drummer;
  }

  public Band drummer(AbstractDrummer drummer) {
    this.drummer = drummer;
    return this;
  }

  public AbstractGuitarist getGuitarist() {
    return guitarist;
  }

  public Band guitarist(AbstractGuitarist guitarist) {
    this.guitarist = guitarist;
    return this;
  }

  public AbstractVocalist getVocalist() {
    return vocalist;
  }

  public Band vocalist(AbstractVocalist vocalist) {
    this.vocalist = vocalist;
    return this;
  }
}
