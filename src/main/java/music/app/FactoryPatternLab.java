package music.app;

import music.band.Band;
import music.factories.AbstractFactory;

/**
 * Created by prayagupd
 * on 4/14/15.
 */

public class FactoryPatternLab {
  public static void main (String[] args){
    Band band = buildMetalBand();
  }

  private static Band buildMetalBand() {
    AbstractFactory metalAbstractFactory = SimpleFactory.getAbstractFactory("METAL");
    Band band = new Band().drummer(metalAbstractFactory.getDrummer())
                          .guitarist(metalAbstractFactory.getGuitarist())
                          .vocalist(metalAbstractFactory.getVocalist());

    return band;
  }
}
