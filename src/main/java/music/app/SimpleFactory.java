package music.app;

import music.factories.AbstractFactory;
import music.factories.BluesAbstractFactory;
import music.factories.MetalAbstractFactory;

/**
 * Created by prayagupd
 * on 4/14/15.
 */

public class SimpleFactory {
  public static AbstractFactory getAbstractFactory(final String type){
    if ("METAL".equals(type)) {
      return new MetalAbstractFactory();
    }
    if ("BLUES".equals(type)) {
      return new BluesAbstractFactory();
    }
    throw  new IllegalArgumentException("No such type exists.");
  }
}
