package music.factories;

import music.product.AbstractDrummer;
import music.product.AbstractGuitarist;
import music.product.AbstractVocalist;
import music.product.BluesDrummer;
import music.product.BluesGuitarist;
import music.product.BluesVocalist;
import music.product.MetalDrummer;
import music.product.MetalGuitarist;
import music.product.MetalVocalist;

/**
 * Created by prayagupd
 * on 4/14/15.
 */

public class BluesAbstractFactory implements AbstractFactory {

  @Override
  public AbstractDrummer getDrummer() {
    return new BluesDrummer();
  }

  @Override
  public AbstractGuitarist getGuitarist() {
    return new BluesGuitarist();
  }

  @Override
  public AbstractVocalist getVocalist() {
    return new BluesVocalist();
  }
}
