package music.factories;

import music.product.AbstractDrummer;
import music.product.AbstractGuitarist;
import music.product.AbstractVocalist;
import music.product.Artist;

/**
 * Created by prayagupd
 * on 4/14/15.
 */

public interface AbstractFactory {
  public AbstractDrummer getDrummer();
  public AbstractGuitarist getGuitarist();
  public AbstractVocalist getVocalist();
  //public Artist getKeyboardist();
}
