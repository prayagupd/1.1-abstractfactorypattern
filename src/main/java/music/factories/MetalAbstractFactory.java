package music.factories;

import music.product.AbstractDrummer;
import music.product.AbstractGuitarist;
import music.product.AbstractVocalist;
import music.product.Artist;
import music.product.MetalDrummer;
import music.product.MetalGuitarist;
import music.product.MetalVocalist;

/**
 * Created by prayagupd
 * on 4/14/15.
 */

public class MetalAbstractFactory implements AbstractFactory {

  @Override
  public AbstractDrummer getDrummer() {
    return new MetalDrummer();
  }

  @Override
  public AbstractGuitarist getGuitarist() {
    return new MetalGuitarist();
  }

  @Override
  public AbstractVocalist getVocalist() {
    return new MetalVocalist();
  }

  //@Override
  //public Artist getKeyboardist() {
  //  return new Keyboardist();
  //}
}
